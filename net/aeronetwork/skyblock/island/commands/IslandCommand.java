package net.aeronetwork.skyblock.island.commands;

import com.google.common.collect.Lists;
import net.aeronetwork.core.command.Command;
import net.aeronetwork.core.utils.FM;
import net.aeronetwork.skyblock.Skyblock;
import net.aeronetwork.skyblock.command.SubCommand;
import net.aeronetwork.skyblock.island.Island;
import net.aeronetwork.skyblock.island.IslandGenerator;
import net.aeronetwork.skyblock.island.commands.subcommands.IslandInviteCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.List;

public class IslandCommand extends Command {

    private List<SubCommand> subCommandList = Lists.newArrayList();

    public IslandCommand() {
        super("island", new String[] {"is"}, "", "/is", "aero.all");
        setPlayerOnly(true);
        subCommandList.add(new IslandInviteCommand());
    }

    @Override
    public void execute(CommandSender caller, String[] args) {
        caller.class.getDeclaredField("")
        Player player = (Player) caller;
        if(Skyblock.getInstance().getIslandManager().hasIsland(player.getUniqueId())) {
            if(isSubCommand(args[0])) {
                executeSubCommand(caller, args[0], String.join(" ", args).replaceFirst(args[0], "").split(" "));
            }
        } else {
            player.sendMessage(FM.mainFormat("Skyblock", "Generating a new Island..."));
            Island island = new IslandGenerator(Skyblock.getInstance().getIslandManager().getIslandsFolder()).generateIsland(player.getUniqueId(), new File("island.schematic"));
            Skyblock.getInstance().getIslandManager().getIslandCache().put(player.getUniqueId(), island);
            player.teleport(island.getSpawnPoint());
            player.sendMessage(FM.mainFormat("Skyblock", "Successfully created a new Island!"));
        }
    }

    public boolean isSubCommand(String command) {
        return subCommandList.stream().anyMatch(subCommand -> subCommand.getCommand().equalsIgnoreCase(command));
    }

    public void executeSubCommand(CommandSender caller, String command, String[] args) {
        subCommandList.stream().filter(subCommand -> subCommand.getCommand().equalsIgnoreCase(command)).findFirst().get().execute(caller, args);
    }
}