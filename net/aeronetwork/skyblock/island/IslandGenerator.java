package net.aeronetwork.skyblock.island;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.justugh.aethra.core.location.LocationUtil;
import net.aeronetwork.core.utils.BlockUtil;
import net.aeronetwork.skyblock.Skyblock;
import net.aeronetwork.skyblock.utils.SchematicUtil;
import org.apache.commons.lang3.Validate;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class IslandGenerator {

    private File islandsFile;

    public IslandGenerator(File islandsFile) {
        Validate.notNull(islandsFile, "[Island Generator]: Islands file cannot be null.");
        this.islandsFile = islandsFile;
    }

    public Island generateIsland(UUID uuid, File schematic) {
        File file = new File(islandsFile.getAbsolutePath() + File.separator + uuid + ".json");

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int nextX = Skyblock.getInstance().getIslandManager().getNextIslandX();

        Location bottomCorner = new Location(Skyblock.getInstance().getIslandManager().getIslandWorld(), nextX, 0, nextX);
        bottomCorner.getBlock().setType(Material.REDSTONE_BLOCK);
        Location topCorner = new Location(Skyblock.getInstance().getIslandManager().getIslandWorld(), nextX + 200, 255, nextX + 200);
        topCorner.getBlock().setType(Material.REDSTONE_BLOCK);

        Location middle = new Location(Skyblock.getInstance().getIslandManager().getIslandWorld(), (bottomCorner.getX() + topCorner.getX()) / 2,
                127, (bottomCorner.getZ() + topCorner.getZ()) / 2);

        Location spawn = null;

        SchematicUtil.paste(schematic.getName(), middle);

        for(Block block : BlockUtil.blocksFromTwoPoints(bottomCorner, topCorner)) {
            if(block.getType().equals(Material.COMMAND)) {
                spawn = block.getLocation();
                block.setType(Material.AIR);
            }
        }

        Validate.notNull(bottomCorner, "[Island Generator]: Bottom Corner cannot be null.");
        Validate.notNull(topCorner, "[Island Generator]: Top Corner cannot be null.");
        Validate.notNull(middle, "[Island Generator]: Middle cannot be null.");

        Island island = new Island(uuid, Lists.newArrayList(), LocationUtil.locationToString(spawn), LocationUtil.locationToString(middle), LocationUtil.locationToString(topCorner)
                , LocationUtil.locationToString(bottomCorner), 0, IslandDimension.OVERWORLD);

        try {
            FileWriter fileWriter = new FileWriter(file.getAbsolutePath());
            BufferedWriter out = new BufferedWriter(fileWriter);
            out.write(new GsonBuilder().setPrettyPrinting().create().toJson(island, Island.class));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return island;
    }

}
