package net.aeronetwork.skyblock.island;

import me.justugh.aethra.core.location.LocationUtil;
import org.bukkit.Location;

import java.util.List;
import java.util.UUID;

public class Island {

    private UUID owner;
    private List<UUID> members;
    private String spawnPoint;
    private String center;
    private String topCorner;
    private String bottomCorner;
    private long level;
    private IslandDimension currentDimension;

    public Island(UUID owner, List<UUID> members, String spawnPoint, String center, String topCorner, String bottomCorner, long level, IslandDimension currentDimension) {
        this.owner = owner;
        this.members = members;
        this.spawnPoint = spawnPoint;
        this.center = center;
        this.topCorner = topCorner;
        this.bottomCorner = bottomCorner;
        this.level = level;
        this.currentDimension = currentDimension;
    }

    public UUID getOwner() {
        return owner;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public Location getSpawnPoint() {
        return LocationUtil.stringToLocation(spawnPoint, true);
    }

    public Location getCenter() {
        return LocationUtil.stringToLocation(center, true);
    }

    public Location getTopCorner() {
        return LocationUtil.stringToLocation(topCorner, true);
    }

    public Location getBottomCorner() {
        return LocationUtil.stringToLocation(bottomCorner, true);
    }

    public long getLevel() {
        return level;
    }

    public IslandDimension getCurrentDimension() {
        return currentDimension;
    }
}
