package net.aeronetwork.skyblock.command;

import org.bukkit.command.CommandSender;

public abstract class SubCommand {

    private String command;

    public SubCommand(String command){
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public abstract void execute(CommandSender caller, String[] args);
}
