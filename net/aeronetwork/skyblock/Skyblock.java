package net.aeronetwork.skyblock;

import net.aeronetwork.core.Core;
import net.aeronetwork.skyblock.island.IslandManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Skyblock extends JavaPlugin {

    private static Skyblock instance;

    private IslandManager islandManager;

    @Override
    public void onEnable() {
        instance = this;
        loadManagers();
    }

    @Override
    public void onDisable() {

    }

    private void loadManagers() {
        islandManager = new IslandManager();
    }

    public static Skyblock getInstance() {
        return instance;
    }

    public IslandManager getIslandManager() {
        return islandManager;
    }
}