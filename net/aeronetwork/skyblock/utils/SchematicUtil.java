package net.aeronetwork.skyblock.utils;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.regions.CuboidRegion;
import net.aeronetwork.core.utils.BlockUtil;
import net.aeronetwork.skyblock.Skyblock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class SchematicUtil {

    public static File save(Player player, String schematicName)
    {
        try
        {
            File schematic = new File(Skyblock.getInstance().getDataFolder(), "/schematics/" + schematicName);
            File dir = new File(Skyblock.getInstance().getDataFolder(), "/schematics/");

            if (!dir.exists()) {
                dir.mkdirs();
            }

            com.sk89q.worldedit.regions.Region selection = FaweAPI.wrapPlayer(player).getSelection();
            Schematic schem = new Schematic(selection);
            schem.save(schematic, ClipboardFormat.SCHEMATIC);

            return schematic;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static File save(String schematicName, Location cornerA, Location cornerB) {
        try {
            File schematic = new File(Skyblock.getInstance().getDataFolder(), "/schematics/" + schematicName);
            File dir = new File(Skyblock.getInstance().getDataFolder(), "/schematics/");

            if (!dir.exists()) {
                dir.mkdirs();
            }

            CuboidRegion selection = new CuboidRegion(FaweAPI.getWorld(cornerA.getWorld().getName()), new Vector(cornerA.getX(), cornerA.getY(), cornerA.getZ()), new Vector(cornerB.getX(), cornerB.getY(), cornerB.getZ()));
            Schematic schem = new Schematic(selection);
            schem.save(schematic, ClipboardFormat.SCHEMATIC);

            return schematic;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Location paste(String schematicName, Location pasteLoc) {
        try {
            File schematic = new File(Skyblock.getInstance().getDataFolder(), "/schematics/" + schematicName);

            Schematic schem = ClipboardFormat.SCHEMATIC.load(schematic);

            schem.paste(FaweAPI.getWorld(pasteLoc.getWorld().getName()),
                    new Vector(pasteLoc.getX(), pasteLoc.getY(), pasteLoc.getZ()), true, true, null);

            return new Location(pasteLoc.getWorld(),
                    pasteLoc.getBlockX() +
                            schem.getClipboard().getMaximumPoint().getBlockX() -
                            schem.getClipboard().getOrigin().getBlockX(),
                    pasteLoc.getBlockY() +
                            schem.getClipboard().getMaximumPoint().getBlockY() -
                            schem.getClipboard().getOrigin().getBlockY(),
                    pasteLoc.getBlockZ() +
                            schem.getClipboard().getMaximumPoint().getBlockZ() -
                            schem.getClipboard().getOrigin().getBlockZ());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
